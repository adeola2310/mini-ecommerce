import React from 'react';
import Product from './Product';
import Details from './Details';
import { storeProducts} from "../data";

export default class ProductList extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            products: storeProducts,
            data: 'Hello WOrld'

        }

    }


    render() {
        console.log(this.state.products);
        console.log(this.state.detailProducts);
        let product = this.state.products.map((storeProducts) => {
            return(
                <Product storeProducts={storeProducts} key={storeProducts.id} />
            );
        });
        return(
          <React.Fragment>
          <div className="py-5">
              <div className="container">
                  <div className="row">
                      <div className="col-10 mx-auto text-center"><h3> Our Products</h3></div>
                      {product}
                  </div>
              </div>
          </div>
          </React.Fragment>
        );
    }
}

