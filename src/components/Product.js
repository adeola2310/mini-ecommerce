import React from 'react';
import {Link} from "react-router-dom";

class Product extends React.Component {

    constructor (props){
        super(props);
    }

    componentDidMount() {
        console.log('product ', this.props.storeProducts);
    }
  addToCart = () =>{
console.log('added');
  }

    render() {
        return (

                <div className="col-sm-6 col-md-4 col-lg-3 mt-4">
                    <div className="card card-inverse card-info">
                        <Link to="/details">
                            <img className="card-img-top" src={this.props.storeProducts.img} />
                        </Link>
                        <div className="card-block">

                            <h4 className="card-title mt-3">{this.props.storeProducts.title}</h4>
                            <div className="meta card-text">
                                <a>Friends</a>
                            </div>
                        </div>
                        <div className="card-footer">
                            <small>${this.props.storeProducts.price}</small>
                            <button className="btn btn-info float-right btn-sm" onClick={this.addToCart()}>Add to Cart</button>
                        </div>
                    </div>
                </div>

        );
    }
}


export default Product;